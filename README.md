# README #

##Sales Tracker Data Application##

### Summary ###

* This is an android application designed specifically to send information to a server through internet access. This application sends Imei no, Model no, Operator and Country_iso of android smart phone to server 202.166.205.39, which uses the following url "http://202.166.205.39/sales_tracker/insert.php". It reads dumpsys file to retrieve IMEI information.
* Version 3.0
* Git Remote Repo: https://shwetastha1@bitbucket.org/shwetastha1/sales_tracker_august_version

### Requirements ###

* Sim should be present in the sim slot.
* App Only sends information once after bootcomplete.
* Application toggle should be turned on.
* Data Network should be working, either Wifi or Data from sim.

### Who do I talk to? ###

* Repo Owner: Shweta Shrestha(shweta.stha1@gmail.com)